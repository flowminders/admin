<?php

Route::group(['middleware' => ['flowcontrol.admin']], function(){

    Route::group(['middleware' => 'flowcontrol.auth'], function(){
        Route::get('/', [
            'as'    => 'admin.dashboard.index',
            'uses'  => 'DashboardController@index'
        ]);

        Route::get('/language/{code}', [
            'as'    => 'admin.changeLang',
            'uses'  => function($code){
                Localizator::set($code);
                return back();
            }
        ]);
    });
    
    /**
     * Auth routes
     */
    Route::get('auth/login', [
        'as'   => 'admin.auth.login',
        'uses' => 'AuthController@getLogin'
    ]);

    Route::post('auth/login', [
        'as'   => 'admin.auth.login',
        'uses' => 'AuthController@postLogin'
    ]);

    Route::get('auth/logout', [
        'as'   => 'admin.auth.logout',
        'uses' => 'AuthController@getLogout'
    ]);
});