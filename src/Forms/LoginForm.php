<?php

namespace FlowControl\Forms;

use Kris\LaravelFormBuilder\Form;

class LoginForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('email', 'text', [
                'label' => trans('flowcontrol::users.email'),
                'rules' => 'required|email',
            ])
            ->add('password', 'password', [
                'label' => trans('flowcontrol::users.password'),
                'rules' => 'required|min:6'
            ])
            ->add('login', 'submit', [
                'label' => trans('flowcontrol::users.login')
            ]);
    }
}