<?php

namespace FlowControl\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class MakeAdminController extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'flowcontrol:controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an admin controller class.';

    protected $type = 'Admin controller class';

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function fire()
    {
        $inputName = $this->getNameInput();
        $noControllerName = str_replace('Controller', '', $inputName);
        $singular = str_singular($noControllerName);
        $plural = str_plural($singular) . 'Controller';

        $name = $this->parseName($plural);

        $path = $this->getPath($name);

        if ($this->alreadyExists($plural)) {
            $this->error($this->type.' already exists!');

            return false;
        }

        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($name));

        $this->info($this->type.' created successfully.');
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = str_replace_first($this->laravel->getNamespace(), '', $name);

        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

    /**
     * Parse the name and format according to the root namespace.
     *
     * @param  string  $name
     * @return string
     */
    protected function parseName($name)
    {
        $rootNamespace = $this->laravel->getNamespace();

        if (starts_with($name, $rootNamespace)) {
            return $name;
        }

        if (str_contains($name, '/')) {
            $name = str_replace('/', '\\', $name);
        }

        return $this->parseName($this->getDefaultNamespace(trim($rootNamespace, '\\')).'\\Admin\\'.$name);
    }

    protected function replaceClass($stub, $name)
    {
        $stub = parent::replaceClass($stub, $name);

        $noControllerName = str_replace('Controller', '', $this->getNameInput());
        $nameSingular = str_singular($noControllerName);
        $namePlural = str_plural($nameSingular);

        $dummyRoute = config('admin.prefix') . '.' . str_plural(
            strtolower( snake_case( $nameSingular, '-' ) )
        );
        $stub = str_replace('dummyroute', $dummyRoute, $stub);

        $appNamespace = $this->getLaravel()->getNamespace();

        $dummyModel = $nameSingular;
        $dummyModelNamespaced = $appNamespace . 'Models\\' . $dummyModel;
        $stub = str_replace('DummyModelNamespaced', $dummyModelNamespaced, $stub);
        $stub = str_replace('DummyModel', $dummyModel, $stub);

        $dummyForm = $nameSingular . 'Form';
        $dummyFormNamespaced = $appNamespace . 'Http\\Forms\\' . $dummyForm;
        $stub = str_replace('DummyFormNamespaced', $dummyFormNamespaced, $stub);
        $stub = str_replace('DummyForm', $dummyForm, $stub);

        $dummyListView = $namePlural . 'ListView';
        $dummyListViewNamespaced = $appNamespace . 'Http\\ListViews\\' . $dummyListView;
        $stub = str_replace('DummyListViewNamespaced', $dummyListViewNamespaced, $stub);
        $stub = str_replace('DummyListView', $dummyListView, $stub);

        $viewPath = config('admin.viewPath');
        if(strlen($viewPath) > 0) {
            $viewPath .= '.';
        }

        $dummyView = $viewPath . str_plural( snake_case(class_basename($nameSingular), '-') );
        $stub = str_replace('dummyview', $dummyView, $stub);

        return $stub;
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        if( $this->option('translated') )
        {
            return __DIR__.'/stubs/admin_controller_translated.stub';
        }

        return __DIR__.'/stubs/admin_controller.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\Controllers';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the admin controller class.'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['translated', 't', InputOption::VALUE_NONE, 'Create a new admin controller that uses a translated model.'],
        ];
    }
}