@extends('flowcontrol::layout.auth')

@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>Flow</b>Admin</a>
        </div><!-- /.login-logo -->
        <div class="login-box-body">

            {!! form($form) !!}

            <a href="#">I forgot my password</a><br>
            <a href="#" class="text-center">Register a new membership</a>

        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script src="/vendor/flowcontrol/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="/vendor/flowcontrol/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/flowcontrol/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
@stop