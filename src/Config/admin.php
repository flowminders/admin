<?php

return [
    'prefix'    => 'admin',
    'viewPath' => 'admin',           // Set this to place admin views in a separate directory
    'modules'   => [
        'form',
    ],
    'sidebars'  => [
        \App\Sidebars\AppSidebar::class,
    ],
    'hasLanguages' => true,
    'storeSuccessMessage' => 'Успешно създаден нов запис',
    'storeErrorMessage' => 'Неуспешно създаден нов запис',
    'updateSuccessMessage' => 'Успешна редакция',
    'updateErrorMessage' => 'Неуспешна редакция',
];