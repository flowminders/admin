<?php

return [
    'prefix'    => 'admin',
    'viewPath' => 'admin',           // Set this to place admin views in a separate directory
    'default'   => [
        'test',
        'test2'
    ],

];