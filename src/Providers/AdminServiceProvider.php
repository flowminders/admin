<?php

namespace FlowControl\Providers;


use FlowControl\Commands\MakeAdmin;
use FlowControl\Middleware\FlowcontrolAuth;
use FlowControl\Commands\MakeAdminController;
use FlowControl\Commands\MakeAdminSeeder;
use FlowControl\Commands\MakeAdminModel;
use FlowControl\ListView\ListViewServiceProvider;
use FlowControl\QueryFilters\QueryFiltersServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Laracasts\Flash\Flash;
use Laracasts\Flash\FlashServiceProvider;

class AdminServiceProvider extends ServiceProvider
{

    private $providers = [
        RoutesServiceProvider::class,
        FlashServiceProvider::class,
        ListViewServiceProvider::class,
        QueryFiltersServiceProvider::class,
    ];

    private $facades = [
        'Flash'         => Flash::class,
    ];

    private $commands = [
        MakeAdminSeeder::class,
        MakeAdminController::class,
        MakeAdminModel::class,
        MakeAdmin::class,
    ];

    private $middleware = [
    ];

    private $middlewareGroups = [
        'flowcontrol.admin' => [
            \Illuminate\Session\Middleware\StartSession::class,
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
        'flowcontrol.auth' => [
            FlowcontrolAuth::class,
        ],
    ];

    private $routeMiddleware = [
        'flowcontrol.auth'        => FlowcontrolAuth::class,
    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerProviders();
        $this->registerFacades();

        $this->commands($this->commands);

        $this->mergeConfigFrom(__DIR__ . '/../Config/admin.php', 'flowcontrol');

        $this->mergeConfigFrom(__DIR__ . '/../Config/admin_sidebar.php', 'admin_sidebar');
    }

    public function boot(Kernel $kernel, Router $router)
    {
        $this->loadViewsFrom(__DIR__ . '/../Views', 'flowcontrol');

        $this->loadTranslationsFrom(__DIR__ . '/../Lang', 'flowcontrol');

        $this->loadMigrationsFrom(__DIR__.'/../Migrations');

        $this->publishers();

        $this->registerMiddlewares($kernel);
        $this->registerGroupMiddlewares($router);
        $this->registerRouteMiddlewares($router);

    }

    private function publishers()
    {
        $this->publishes([
            __DIR__ . '/../Views' => resource_path('views/vendor/flowcontrol')
        ], 'views');

        $this->publishes([
            __DIR__ . '/../Config/admin.php' => config_path('admin.php')
        ], 'config');

        $this->publishes([
            __DIR__ . '/../Assets' => public_path('vendor/flowcontrol'),
        ], 'public');
    }

    /**
     * @param Kernel $kernel
     */
    private function registerMiddlewares(Kernel $kernel)
    {
        foreach ($this->middleware as $middleware)
        {
            $kernel->pushMiddleware($middleware);
        }
    }

    private function registerGroupMiddlewares($router)
    {
        foreach($this->middlewareGroups as $groupName => $middlewareGroup)
        {
            $router->middlewareGroup($groupName, $middlewareGroup);
        }
    }

    private function registerRouteMiddlewares(Router $router)
    {
        foreach($this->routeMiddleware as $key => $middleware)
        {
            $router->middleware($key, $middleware);
        }
    }

    private function registerProviders()
    {
        foreach ($this->providers as $provider)
        {
            $this->app->register($provider);
        }
    }

    private function registerFacades()
    {
        AliasLoader::getInstance($this->facades)->register();
    }
}