<?php

namespace FlowControl\Controllers;

use Illuminate\Routing\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view('flowcontrol::dashboard.index');
    }
}