# Basic usage

* Install using composer:
```
composer require flowcontrol/admin
```

* Register the Service Provider (app/Providers/AppServiceProvider.php):
```php
// in AppServiceProvider
public function register()
{
    $this->app->register(\FlowControl\Providers\AdminServiceProvider::class);
}

```

* Publish assets, configs, migrations and etc.
```
php artisan vendor:publish --provider="FlowControl\Providers\AdminServiceProvider"
```

* Migrate the base tables
```
php artisan migrate
```